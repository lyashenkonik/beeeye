$(document).ready(function(){
    var is_chrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);

    //slider
    $('.slides').slick({
        dots: true,
        speed: 500,
        fade: true,
        prevArrow: $('.prev'),
        nextArrow: $('.next')
    });
    //end slider

    $('.mouse').click(function () {
        $('html, body').animate({ scrollTop: $('.integration').offset().top }, 500);
    })

    if(is_chrome){
        $('.lineImg').attr("src", 'img/line-lg.png');
    }else {
        $('.lineImg').attr("src", 'img/lineFire.png');
    }

    if (/Edge\/\d./i.test(navigator.userAgent)){
        $('.lineImg-md').attr("src", 'img/line-md-ie.png');
        $('.lineImg-md').css("left", '-4px');
    }else {
        $('.lineImg-md').attr("src", 'img/line-md.png');
    }

    $('#lineLg').css("height", '0');
    $('#lineMd').css("height", '0');

    $(window).scroll(function(){
        var scrollTop = $(window).scrollTop();

        if(scrollTop < 400) {
            $('#lineLg').css("height", '0');
            $('#lineMd').css("height", '0');
        }else {
            $('#lineLg').css("height", scrollTop-120 + 'px');
            $('#lineMd').css("height", scrollTop-120 + 'px');
        }
    })

    //Send form region
    $('.success').hide()
    $('.error').hide()

    $("#form").submit(function() {
        ga('send', 'event', 'form');
        yaCounter46099212.reachGoal('request_form');
        var form_data = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "send.php",
            data: form_data,
            error: function() {
                alert("Internet error");
            },
            success: function(result) {
                if (result == 200) {
                    $('.error').hide()
                    $('.success').show()
                } else {
                    $('.success').hide()
                    $('.error').show()
                }
            }
        });

        return false;
    });

});