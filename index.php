<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Хэндисофт: beeEYE – решение задач бизнес-анализа и отчетности</title>
    <meta name="description" content="Информационно-аналитическая система для решения задач интеграции, управления качеством данных, бизнес-анализа и визуализации" />
    <meta name="keywords" content="анализ данных, data mining, big data, большие данные, визуализация данных, бизнес анализ, бизнес аналитика, отечественное по, ETL, extract transform load, OLAP, OLAP куб, 1С:Предприятие, хранилища данных, data warehouse, OLAP анализ, business intelligence, bi" />
    <!-- Facebook Open Graph -->
    <meta property="og:title" content="Хэндисофт: beeEYE – решение задач бизнес-анализа и отчетности" />
    <meta property="og:description" content="Информационно-аналитическая система для решения задач интеграции, управления качеством данных, бизнес-анализа и визуализации" />
    <meta property="og:image" content="http://www.beeeye.ru/img/meta_image.png" />
    <meta property="og:site_name" content="Хэндисофт: beeEYE" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="http://www.beeeye.ru/img/meta_image.png" />
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="630">
    <!-- Facebook Open Graph End -->
    <!-- Twitter Card -->
    <meta property="twitter:title" content="Хэндисофт: beeEYE – решение задач бизнес-анализа и отчетности" />
    <meta property="twitter:description" content="Информационно-аналитическая система для решения задач интеграции, управления качеством данных, бизнес-анализа и визуализации" />
    <meta property="twitter:image:src" content="http://www.beeeye.ru/img/meta_image.png" />
    <meta property="twitter:card" content="summary_large_image" />
    <meta property="twitter:domain" content="www.beeeye.ru" />
    <!-- Twitter Card End -->

    <link rel="apple-touch-icon" sizes="57x57" href="icon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="icon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="icon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="icon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="icon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="icon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="icon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="icon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="icon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="icon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="icon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="icon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="icon/favicon-16x16.png">
    <link rel="manifest" href="icon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="icon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" href="css/app.css">
<!--    <link rel="stylesheet" href="css/reset.css">-->
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/slick.css">
    <link rel="stylesheet" href="css/slick-theme.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>

<div class="container">

    <div class="section header">
<!--        <div class="logo">-->
<!--            <img class="img-responsive" src="img/logo.svg" alt="Логотип">-->
<!--        </div>-->

        <div class="row">

            <div class="wrapper center-block">
                <div class="slides">

                    <div class="item">
                        <img class="center-block" src="img/slider/0.svg" alt="">

                        <div class="text center-block">
                            <strong>BeeEYE</strong> – комплексный модульный продукт для решения задач интеграции, управления качеством данных, бизнес-анализа и визуализации.
                        </div>
                    </div>

                    <div class="item">
                        <img class="center-block" src="img/slider/1.png" alt="">

                        <div class="text center-block">
                            Интеграция данных из различных источников.
                        </div>
                    </div>

                    <div class="item">
                        <img class="center-block" src="img/slider/2.png" alt="">

                        <div class="text center-block">
                            Настройка и контроль работы ETL-процессов.
                        </div>
                    </div>

                    <div class="item">
                        <img class="center-block" src="img/slider/3.png" alt="">

                        <div class="text center-block">
                            Трансформация данных. <br>
                            Приведение информации к виду необходимому для ее анализа и обработки.
                        </div>
                    </div>

                    <div class="item">
                        <img class="center-block" src="img/slider/4.png" alt="">

                        <div class="text center-block">
                            Трансформация данных. <br>
                            Настройка многоэтапных сценариев обработки данных.
                        </div>
                    </div>

                    <div class="item">
                        <img class="center-block" src="img/slider/5.jpg" alt="">

                        <div class="text center-block">
                            Полиматика <br>
                            Многомерный анализ данных, методы продвинутой аналитики, data mining.
                        </div>
                    </div>

                    <div class="item">
                        <img class="center-block" src="img/slider/6.png" alt="">

                        <div class="text center-block">
                            Конструктор аналитических панелей. <br>
                            Настройка отчетов и панелей визуализации данных в конструкторе отчетов.
                        </div>
                    </div>

                    <div class="item">
                        <img class="center-block" src="img/slider/7.png" alt="">

                        <div class="text center-block">
                            Портал аналитической отчетности. <br>
                            Работайте в привычном браузере с любого устройства от настольного компьютера до смартфона
                        </div>
                    </div>

                    <div class="item">
                        <img class="center-block" src="img/slider/8.png" alt="">

                        <div class="text center-block">
                            Портал аналитической отчетности. <br>
                            Работайте в привычном браузере с любого устройства от настольного компьютера до смартфона
                        </div>
                    </div>

                    <div class="item">
                        <img class="center-block" src="img/slider/9.png" alt="">

                        <div class="text center-block">
                            Портал аналитической отчетности. <br>
                            Работайте в привычном браузере с любого устройства от настольного компьютера до смартфона
                        </div>
                    </div>

                    <div class="item">
                        <img class="center-block" src="img/slider/10.png" alt="">

                        <div class="text center-block">
                            Портал аналитической отчетности. <br>
                            Работайте в привычном браузере с любого устройства от настольного компьютера до смартфона
                        </div>
                    </div>

                </div><!--ends.slides-->

                <div class="tablet">
                    <img class="center-block hidden-xs hidden-sm" src="img/slider/tablet.svg" alt="">
                </div>

                <div class="arrows">
                    <img class="pull-left prev" src="img/icon/arrow.svg" alt="">

                    <img class="pull-right next" src="img/icon/arrow.svg" alt="">
                </div>

                <div class="mouse center-block"></div>
            </div><!--ends.wrapper-->

        </div><!--ends.row-->

        <div class="hr-dashed"></div>
    </div><!--ends.section header-->

    <div class="line" id="lineLg">
        <img class="visible-lg relative-center lineImg" alt="line">
    </div>

    <div class="line lineMd" id="lineMd">
        <img class="visible-md relative-center lineImg-md" alt="line">
    </div>

    <div class="section integration">

        <div class="row">

            <div class="col-md-6 image wow zoomIn">
                <img class="center-block img-responsive" src="img/content/1.svg" alt="Интеграция данных">
            </div><!--ends.col-md-6 img content-->

            <div class="col-md-6 text-block wow fadeInUpBig">

                <h1 class="title">
                    Интеграция данных
                </h1>
                <h2 class="text">
                    Ключевой особенностью продукта является возможность гибкой настройки получения данных из систем на платформе «1С:Предприятие», в том числе размещенных в облачном сервисе.
                </h2>
            </div><!--ends.col-md-6 textContent-->

            <div class="col-md-5 col-md-offset-6 wow fadeInUpBig">
                <ul class="costume-list">
                    <li>
                        <h3 class="text">
                            Получайте данные из различных источников
                        </h3>
                    </li>

                    <li>
                        <h3 class="text">
                            Настраивайте расписание получения данных
                        </h3>
                    </li>

                    <li>
                        <h3 class="text">
                            Синхронизируйте потоки получения данных
                        </h3>
                    </li>

                    <li>
                        <h3 class="text">
                            Контролируйте процесс получения данных в режиме реального времени
                        </h3>
                    </li>
                </ul>
            </div><!--ends.list-->

        </div><!--ends.row-->


        <div class="hr-dashed"></div>
    </div><!--ends.section integration-->

    <div class="section data-quality">

        <div class="row">

            <div class="col-sm-12 image visible-sm visible-xs">
                <img class="img-responsive" src="img/content/2.svg" alt="Управление качеством данных">
            </div><!--ends.col-md-6 img content-->

            <div class="col-md-6 wow fadeInUpBig">
                <h1 class="title">
                    Управление качеством данных
                </h1>
                <h2 class="text">
                    Качество данных это комплексная характеристика,
                    отражающая степень пригодности данных для анализа.
                    Решения, основанные на анализе некачественных данных,
                    могут привести к непредсказуемым результатам.
                </h2>
            </div><!--ends.col-md-6 textContent-->

            <div class="col-md-6 image visible-lg visible-md wow zoomIn">
                <img class="img-responsive center-block" src="img/content/2.svg" alt="Управление качеством данных">
            </div><!--ends.col-md-6 img content-->

            <div class="col-md-5 col-md-offset-4 wow fadeInUpBig">
                <ul class="costume-list">
                    <li>
                        <h3 class="text">
                            Будьте уверены в актуальности, целостности и непротиворечивости ваших данных
                        </h3>
                    </li>

                    <li>
                        <h3 class="text">
                            Настраивайте процедуры технического контроля полученных данных, а также проверки на соответствие определенным бизнес-правилам
                        </h3>
                    </li>

                    <li>
                        <h3 class="text">
                            Управляйте многоэтапными сценариями автоматизированной проверки данных
                        </h3>
                    </li>

                    <li>
                        <h3 class="text">
                            Выявляйте ошибки ввода данных в системах-источниках
                        </h3>
                    </li>
                </ul>
            </div><!--ends.list-->

        </div><!--ends.row-->

        <div class="hr-dashed"></div>
    </div><!--ends.section data-quality-->

    <div class="section control">

        <div class="row">

            <div class="col-md-6 image wow zoomIn">
                <img class="center-block img-responsive" src="img/content/3.svg" alt="Управление качеством данных">
            </div><!--ends.col-md-6 img content-->

            <div class="col-md-6 text-block wow fadeInUpBig">
                <h1 class="title">
                    Управление хранилищем данных
                </h1>
                <h2 class="text">
                    Качественно спроектированное хранилище данных обеспечивает «Единую версию правды» и наполняет новым смыслом разрозненные прежде массивы данных.
                </h2>
            </div><!--ends.col-md-6 textContent-->

            <div class="col-md-5 col-md-offset-6 wow fadeInUpBig">
                <ul class="costume-list">

                    <li>
                        <h3 class="text">
                            Моделируйте и управляйте единым репозиторием данных
                        </h3>
                    </li>

                    <li>
                        <h3 class="text">
                            Обеспечивайте сохранение исторических данных и непрерывность отчетности при замене систем-источников
                        </h3>
                    </li>

                    <li>
                        <h3 class="text">
                            Реализуйте аудиторский след изменений данных в системах-источниках (если его там нет)
                        </h3>
                    </li>

                    <li>
                        <h3 class="text">
                            Формируйте многомерные модели данных для последующего исследования и аналитической обработки больших массивов информации
                        </h3>
                    </li>
                    <li>
                        <h3 class="text">
                            Формируйте витрины данных для доступной визуализации и построения системы регулярной отчетности
                        </h3>
                    </li>
                </ul>
            </div><!--ends.list-->

        </div><!--ends.row-->

        <div class="hr-dashed"></div>
    </div><!--ends.section control-->

    <div class="section analytics">

        <div class="row">

            <div class="col-sm-12 image visible-sm visible-xs">
                <img class="img-responsive" src="img/content/4.svg" alt="Аналитическая платформа olap">
            </div><!--ends.col-md-6 img content-->

            <div class="col-md-6 wow fadeInUpBig">
                <h1 class="title">
                    Аналитическая платформа<br>olap
                </h1>
                <h2 class="text">
                   Скорость и качество аналитической работы с данными - определяющий фактор успеха в современном мире. Оперативность выявления знаний из большого объема слабосвязанной между собой информации становится основным конкурентным преимуществом в ускоряющемся темпе рыночных изменений.
                </h2>

                <img class="img-responsive polymatica" src="img/polymatica-03.svg" alt="polymatica logo">
            </div><!--ends.col-md-6 textContent-->

            <div class="col-md-6 image visible-lg visible-md wow zoomIn">
                <img class="img-responsive" src="img/content/4.svg" alt="Управление качеством данных">
            </div><!--ends.col-md-6 img content-->

            <div class="col-md-5 col-md-offset-4 wow fadeInUpBig">
                <ul class="costume-list">
                    <li>
                        <h3 class="text">
                            Обрабатывайте большие объемы данных в режиме реального времени
                        </h3>
                    </li>

                    <li>
                        <h3 class="text">
                            Осуществляйте многомерный анализ данных в различных разрезах
                        </h3>
                    </li>

                    <li>
                        <h3 class="text">
                            Производите расчеты на полном объеме данных «на лету»
                        </h3>
                    </li>

                    <li>
                        <h3 class="text">
                            Применяйте методы продвинутой аналитики при анализе Big Data
                        </h3>
                    </li>

                    <li>
                        <h3 class="text">
                            Публикуйте преднастроенные интерактивные отчеты
                        </h3>
                    </li>

                    <li>
                        <h3 class="text">
                            Экспортируйте отчеты в различные форматы (xlsx, pdf, jpeg)
                        </h3>
                    </li>
                </ul>
            </div><!--ends.list-->

        </div><!--ends.row-->


        <div class="hr-dashed"></div>
    </div><!--ends.section analytics-->

    <div class="section constructor">

        <div class="row">

            <div class="col-md-6 image wow zoomIn">
                <img class="center-block img-responsive" src="img/content/5.svg" alt="Конструктор отчетов">
            </div><!--ends.col-md-6 img content-->

            <div class="col-md-6 text-block wow fadeIn fadeInUpBig">
                <h1 class="title">
                    Конструктор аналитических панелей
                </h1>
                <h2 class="text">
                    Простой и понятный интерфейс позволяет создавать макеты отчетов и панелей управления (dashboards) различной сложности после минимального обучения пользователя.
                </h2>
            </div><!--ends.col-md-6 textContent-->

            <div class="col-md-5 col-md-offset-6 wow fadeInUpBig">
                <ul class="costume-list">
                    <li>
                        <h3 class="text">
                            Подключайтесь к наборам данных в витринах хранилища или загружайте свои данные из файлов
                        </h3>
                    </li>

                    <li>
                        <h3 class="text">
                            Используйте описание и статистику по полям наборов данных для быстрого понимания структуры данных и их представления для визуализации
                        </h3>
                    </li>

                    <li>
                        <h3 class="text">
                            Проектируйте интерактивные отчеты в несколько кликов в удобном интерфейсе
                        </h3>
                    </li>

                    <li>
                        <h3 class="text">
                            Настраивайте адаптивную верстку аналитических панелей для разных разрешений экранов - от компьютерных мониторов до мобильных телефонов
                        </h3>
                    </li>

                    <li>
                        <h3 class="text">
                            Используйте разнообразные типы диаграмм и таблиц для визуализаций данных
                        </h3>
                    </li>

                    <li>
                        <h3 class="text">
                            Выбирайте правила фильтрации, агрегации данных, переходов в другие отчеты, детализации (drill-down).
                        </h3>
                    </li>

                    <li>
                        <h3 class="text">
                            Настраивайте вычисляемые поля, используя визуальный конструктор выражений и собственные функции.
                        </h3>
                    </li>
                </ul>
            </div><!--ends.list-->

        </div><!--ends.row-->


        <div class="hr-dashed"></div>
    </div><!--ends.section constructor-->

    <div class="section report">

        <div class="row">

            <div class="col-sm-12 image visible-sm visible-xs">
                <img class="img-responsive" src="img/content/6.svg" alt="Портал аналитической отчетности">
            </div><!--ends.col-md-6 img content-->

            <div class="col-md-5 wow fadeInUpBig">
                <h1 class="title">
                    Портал аналитической отчетности
                </h1>
                <h2 class="text">
                    Возможность комплексного анализа информации в любое время и в любом месте по различным аспектам деятельности позволяет:
                </h2>
            </div><!--ends.col-md-6 textContent-->

            <div class="col-md-7 image visible-lg visible-md wow zoomIn">
                <img src="img/content/6.svg" alt="Портал аналитической отчетности">
            </div><!--ends.col-md-6 img content-->

            <div class="desc col-md-12">
                <div class="__top gr"></div>
                <div class="row">

                    <div class="col-md-4 item wow fadeIn" data-wow-delay=".5s">
                        <img src="img/icon/1.svg" alt="Руководителю">
                        <h1 class="title">
                            Руководителю
                        </h1>
                        <h2 class="text">
                            оценивать в режиме реального времени эффективность работы предприятия, принимать обоснованные решения, опираясь на достоверную информацию
                        </h2>
                    </div>

                    <div class="col-md-4 item wow zoomInUp">
                        <img src="img/icon/2.svg" alt="Аналитику">
                        <h1 class="title">
                            Аналитику
                        </h1>
                        <h2 class="text">
                            увидеть тенденции, скрытые зависимости в большом объеме информации, строить и проверять гипотезы на реальных данных
                        </h2>
                    </div>

                    <div class="col-md-4 item wow zoomInRight" data-wow-delay=".5s">
                        <img src="img/icon/3.svg" alt="Специалисту">
                        <h1 class="title">
                            Специалисту
                        </h1>
                        <h2 class="text">
                            упростить и ускорить процесс подготовки регулярной отчетности для руководителей всех уровней.
                        </h2>
                    </div>

                </div><!--ends.row-->
                <div class="__bottom gr"></div>
            </div><!--ends.desc-->

            <div class="col-md-5 col-md-offset-4 wow fadeInUpBig">
                <ul class="costume-list">
                    <li>
                        <h3 class="text">
                            Работайте в привычном браузере с любого устройства от настольного компьютера до смартфона.
                        </h3>
                    </li>

                    <li>
                        <h3 class="text">
                            Анализируйте данные, выбирая отборы и группировки в панели фильтров, детализируйте данные, переходя от общего к частному и обратно.
                        </h3>
                    </li>

                    <li>
                        <h3 class="text">
                            Экспортируйте отчеты в файлы различных форматов (xlsx, pdf, jpeg)
                        </h3>
                    </li>
                </ul>
            </div><!--ends.list-->

        </div><!--ends.row-->

    </div><!--ends.section analytics-->

</div><!--ends.container-->

<div class="container-fluid" style="position: relative;">
    <div class="section form">
        <form id="form" class="form-horizontal col-md-7 center-block">
            <div class="col-12 text-white">
                <h3 class="success text-center">
                    Спасибо за обращение! <br>
                    Ваше сообщение успешно отправлено. <br>
                    В ближайшее время с Вами свяжется наш специалист.
                </h3>
            </div>
            <div class="form-group">
                <label for="name" class="col-sm-4 control-label">Имя и фамилия</label>
                <div class="col-sm-8">
                    <input type="text" name="name" class="form-control" id="name" required>
                </div>
            </div>

            <div class="form-group">
                <label for="company" class="col-sm-4 control-label">Компания</label>
                <div class="col-sm-8">
                    <input type="text" name="company" class="form-control" id="company" required>
                </div>
            </div>

            <div class="form-group">
                <label for="email" class="col-sm-4 control-label">E-mail</label>
                <div class="col-sm-8">
                    <input type="email" name="email" class="form-control" id="email" required>
                </div>
            </div>

            <div class="form-group">
                <label for="phone" class="col-sm-4 control-label">Телефон</label>
                <div class="col-sm-8">
                    <input type="text" name="phone" class="form-control" id="phone" required>
                </div>
            </div>

            <div class="form-group mb-0">
                <!-- <label for="capt" class="col-sm-4 control-label">Слово на картнике</label>-->
                <div class="col-sm-8 col-md-offset-4">
                    <div class="g-recaptcha" data-sitekey="6Lc0NTIUAAAAAMjIqvUhZJIYLdn2MdJQAKwUNtkJ"></div>
                    <small class="error text-center">Пройдите проверку</small>
                </div>
            </div>

            <button type="submit" class="costume-btn">Заказать демонстрацию</button>
        </form>
    </div><!--ends.section form-->
</div>

<div class="container">
    <div class="footer">
        <div class="col-md-7 center-block">

            <span class="text-muted text">
                <small>
                    Нажимая на кнопку «Заказать демонстрацию» вы соглашаетесь на обработку персональных данных. Обработка персональных данных осуществляется в соответствии с <a
                            href="https://handy-soft.ru/privacy/" target="_blank">«Политикой конфиденциальности персональных данных».</a>
                </small>
            </span>
        </div>

        <div class="col-md-9 center-block">
            <div class="pull-right">
                <span class="text">+7 (495) 118-28-14</span>
                <img class="email-img" src="img/info-email.png" alt="email">
            </div>
            <div class="pull-left">
                <img src="img/handySoft-logo.svg" class="logo" alt="Handy-soft logo">
            </div>
        </div>
    </div><!--ends.footer-->
</div><!--ends.container-->

<script src="js/sys/jquery-3.2.1.min.js"></script>
<script src="js/app.js"></script>
<script src="js/sys/wow.min.js"></script>
<script src="js/slick.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="js/sys/ie10-viewport-bug-workaround.js"></script>
<script>
    new WOW().init();
</script>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-107206862-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments)};
    gtag('js', new Date());

    gtag('config', 'UA-107206862-1');
</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter46099212 = new Ya.Metrika({
                    id:46099212,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/46099212" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</body>
</html>
